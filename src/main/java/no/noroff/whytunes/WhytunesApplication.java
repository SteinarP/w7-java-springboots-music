package no.noroff.whytunes;

import no.noroff.whytunes.data_access.TrackRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhytunesApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhytunesApplication.class, args);
        System.out.println("Running Spring app.");

        //TrackRepository sql = new TrackRepository();

        /*
        for (Track t : sql.selectRandomTracks(5)){
            System.out.println(t);
        }

        for (Artist a : sql.selectRandomArtists(5)){
            System.out.println(a);
        }

        for (Genre g : sql.selectRandomGenres(5)){
            System.out.println(g);
        }

        System.out.println("Search results:");
        for (Track t : sql.selectTracksByName("")){
            System.out.println(t);
        }
        */
    }

}
