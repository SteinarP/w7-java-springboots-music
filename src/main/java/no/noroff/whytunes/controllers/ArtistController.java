package no.noroff.whytunes.controllers;

import no.noroff.whytunes.data_access.ArtistRepository;
import no.noroff.whytunes.data_access.CustomerRepository;
import no.noroff.whytunes.data_access.TrackRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArtistController {

    CustomerRepository crepo = new CustomerRepository();
    TrackRepository trepo = new TrackRepository();
    ArtistRepository arepo = new ArtistRepository();

    @RequestMapping(value="/api/artists/{id}")
    public String getArtist(@PathVariable String id, Model model){
        model.addAttribute(arepo.selectRandomArtists(1).get(0));
        return "view-artist";
    }
}
