package no.noroff.whytunes.controllers;

import no.noroff.whytunes.data_access.CustomerRepository;
import no.noroff.whytunes.data_access.GenreRepository;
import no.noroff.whytunes.models.Customer;
import no.noroff.whytunes.models.Genre;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    CustomerRepository repo = new CustomerRepository();

    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return repo.getAllCustomers();
        //return customerRepo.getAllCustomers();
    }

    @RequestMapping(value="/api/customers/{id}")
    public Customer getCustomer(@PathVariable String id){
        return repo.getCustomer(Integer.parseInt(id));
    }

    @RequestMapping(value="/api/customers", method=RequestMethod.POST)
    public boolean addNewCustomer(@RequestBody Customer customer){
        return repo.addNewCustomer(customer);
    }

    @RequestMapping(value="/api/customers", method = RequestMethod.PUT)
    public boolean updateCustomer(@RequestBody Customer customer){
        return repo.updateCustomer(customer);
    }

    @RequestMapping(value="/api/customers/top/countries", method=RequestMethod.GET)
    public ArrayList<String> getTopCustomerCountries(){
        return repo.getTopCustomerCountries();
    }

    @RequestMapping(value="/api/customers/top", method=RequestMethod.GET)
    public ArrayList<Customer> getTopSpendingCustomers(){
        return repo.getTopSpendingCustomers();
    }

    @RequestMapping(value = "/api/customers/{id}/top/genre", method = RequestMethod.GET)
    public ArrayList<Genre> getCustomerTopGenres(@PathVariable String id){
        GenreRepository grepo = new GenreRepository();
        return grepo.getCustomerTopGenres(Integer.parseInt(id));
    }
}
