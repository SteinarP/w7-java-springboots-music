package no.noroff.whytunes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    private String URL = "jdbc:sqlite::resource:Chinook.sqlite";

    //Home
    @RequestMapping("/")
    public String index(){
        return "index";
    }

    /*
    @RequestMapping("/artists/{id}")
    public String getArtist(@PathVariable("id") String id){
        System.out.println("Getting artist by id: "+id);
        //Artist a = db.getArtist(id);
        return "Artist";
    }
    */

    @RequestMapping(value="/search", method = RequestMethod.GET)
    public String simpleSearch(@RequestParam("query") String terms){
        return "Search results for "+terms;
    }

}
