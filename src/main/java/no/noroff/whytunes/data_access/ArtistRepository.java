package no.noroff.whytunes.data_access;

import no.noroff.whytunes.models.Artist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ArtistRepository {
    Connection conn = null;

    public ArrayList<Artist> selectArtists(String query) {
        ArrayList<Artist> artists = new ArrayList<Artist>();

        try {
            // Open connection
            conn = DriverManager.getConnection(SQLHelper.URL);

            // Get SQL query
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet results = ps.executeQuery();

            while (results.next()) {
                int id = Integer.parseInt(results.getString("ArtistId"));
                String name = results.getString("Name");
                Artist a = new Artist(id, name);
                artists.add(a);
            }
        } catch (Exception ex) {
            System.err.println("Error occurred.");
            System.err.print(ex.toString());
        } finally {
            SQLHelper.closeConnection(conn);
        }
        return artists;
    }

    public ArrayList<Artist> selectRandomArtists(int amount){
        return selectArtists("SELECT * from Artist ORDER BY RANDOM() LIMIT "+amount);
    }
    public Artist selectArtist(int id){
        return selectArtists("SELECT * from Artist WHERE ArtistId = "+id).get(0);
    }
}
