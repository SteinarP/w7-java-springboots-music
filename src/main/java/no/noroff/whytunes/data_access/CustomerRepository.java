package no.noroff.whytunes.data_access;

import no.noroff.whytunes.models.Customer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CustomerRepository {

    Connection conn = null;

    // === === === === === GET === === === === ===
    // API
    public ArrayList<Customer> selectCustomer(String query){
        ArrayList<Customer> customers = new ArrayList<Customer>();

        try {
            // Open connection
            conn = DriverManager.getConnection(SQLHelper.URL);

            // Get SQL query
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet results = ps.executeQuery();

            while (results.next()) {
                int id = Integer.parseInt(results.getString("CustomerId"));
                Customer c = new Customer(results);
                customers.add(c);
            }
        } catch (Exception ex) {
            System.err.println("Error occurred.");
            System.err.print(ex.toString());
        } finally {
            SQLHelper.closeConnection(conn);
        }
        return customers;
    }

    public ArrayList<Customer> getAllCustomers(){
        return selectCustomer("SELECT * from Customer");
        //id, fname, lname, country, postal, phone
        //return selectCustomer("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone FROM Customer");
    }

    public Customer getCustomer(int id){
        String query = "SELECT * from Customer WHERE CustomerId ="+id;
        /*
        String query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone" +
                " FROM Customer WHERE CustomerId ="+id;
        */
        ArrayList<Customer> singleCustomerList = selectCustomer(query);
        if (singleCustomerList.size() == 0){
            return null;
        }
        return singleCustomerList.get(0);
    }

    // === === === === === POST, PUT === === === === ===
    public boolean addNewCustomer(Customer customer){
        boolean success = false;
        // TODO: SQLiteException: Abort due to constraint violation (NOT NULL constraint failed: Customer.FirstName)

        try {
            // Open connection
            conn = DriverManager.getConnection(SQLHelper.URL);

            // Prepare insert statement query
            String query = "INSERT INTO Customer("+
            "CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email"
                +") VALUES(?,?,?,?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, customer.getCustomerId());
            ps.setString(2, customer.getFirstName());
            ps.setString(3, customer.getLastName());
            ps.setString(4, customer.getCountry());
            ps.setString(5, customer.getPostalCode());
            ps.setString(6, customer.getPhone());
            ps.setString(7, customer.getEmail());
            //Company;Address;City;State;Country;PostalCode;Phone;Fax;Email;int SupportRepId;

            int result = ps.executeUpdate();
            success = (result!=0); //Wrote >0

        } catch (Exception ex) {
            System.err.println("Error occurred in addNewCustomer().");
            System.err.print(ex.toString());
        } finally {
            SQLHelper.closeConnection(conn);
            return success;
        }
    }

    public boolean updateCustomer(Customer customer){
        boolean success = false;

        try {
            // Open connection
            conn = DriverManager.getConnection(SQLHelper.URL);

            // Prepare update statement
            String query = "UPDATE Customer SET"
                    +" CustomerId=?," //1
                    +" FirstName=?," //2
                    +" LastName=?," //3
                    +" Country=?," //4
                    +" PostalCode=?," //5
                    +" Phone=?," //6
                    +" Email=?" //7
                    +" WHERE CustomerId=?"; //8
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, customer.getCustomerId()); //New id?
            ps.setString(2, customer.getFirstName());
            ps.setString(3, customer.getLastName());
            ps.setString(4, customer.getCountry());
            ps.setString(5, customer.getPostalCode());
            ps.setString(6, customer.getPhone());
            ps.setString(7, customer.getEmail());
            ps.setInt(8, customer.getCustomerId()); //Query target

            int result = ps.executeUpdate();
            success = (result!=0); //Wrote >0

        } catch (Exception ex) {
            System.err.println("Error occurred in updateCustomer().");
            System.err.print(ex.toString());
        } finally {
            SQLHelper.closeConnection(conn);
            return success;
        }
    }

    // === === === === === GET EXTRA === === === === ===
    public ArrayList<String> getTopCustomerCountries(){
        ArrayList<Customer> customers = new ArrayList<Customer>();
        ArrayList<String> countries = new ArrayList<String>();

        try {
            // Open connection
            conn = DriverManager.getConnection(SQLHelper.URL);

            // Get SQL query
            String query = "SELECT Country FROM Customer GROUPBY Country ORDERBY COUNT(*) DESC LIMIT 10";
            PreparedStatement ps = conn.prepareStatement(query);
            System.out.println("Ok, executing now");
            ResultSet results = ps.executeQuery();

            while (results.next()) {
                int id = Integer.parseInt(results.getString("CustomerId"));
                Customer c = new Customer(results);
                customers.add(c);
            }
        } catch (Exception ex) {
            System.err.println("Error occurred.");
            System.err.print(ex.toString());
        } finally {
            SQLHelper.closeConnection(conn);
        }

        System.out.println("Top countries:");
        return null;
    }

    public ArrayList<Customer> getTopSpendingCustomers(){
        ArrayList<Customer> customers = new ArrayList<Customer>();
        return customers;
    }
}
