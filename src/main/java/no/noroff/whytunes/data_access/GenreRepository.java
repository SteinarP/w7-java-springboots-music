package no.noroff.whytunes.data_access;

import no.noroff.whytunes.models.Genre;
import no.noroff.whytunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GenreRepository {
    Connection conn = null;

    public ArrayList<Genre> selectGenres(String query) {
        ArrayList<Genre> genres = new ArrayList<Genre>();
        ArrayList<Track> tracks = new ArrayList<Track>();

        try {
            // Open connection
            conn = DriverManager.getConnection(SQLHelper.URL);

            // Get SQL query
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet results = ps.executeQuery();

            while (results.next()) {
                int id = Integer.parseInt(results.getString("GenreId"));
                String name = results.getString("Name");
                Genre g = new Genre(id, name);
                genres.add(g);
            }
        } catch (Exception ex) {
            System.err.println("Error occurred.");
            System.err.print(ex.toString());
        } finally {
            SQLHelper.closeConnection(conn);
        }
        return genres;
    }

    public ArrayList<Genre> selectRandomGenres(int amount){
        return selectGenres("SELECT * from Genre ORDER BY RANDOM() LIMIT "+amount);
    }
    public Genre selectGenre(int id){
        return selectGenres("SELECT * from Genre WHERE GenreId = "+id).get(0);
    }

    public ArrayList<Genre> getCustomerTopGenres(int id){
        //TODO
        /* For a given customer, their most popular genre (tie: both).
         * Most popular meaning the genre corresponding to the most
         * tracks among customer's invoices.
         */
        ArrayList<Genre> genres = new ArrayList<Genre>();
        return genres;
    }
}
