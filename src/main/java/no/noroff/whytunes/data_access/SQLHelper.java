package no.noroff.whytunes.data_access;

import java.sql.Connection;

public class SQLHelper {

    public static final String URL = "jdbc:sqlite::resource:Chinook.sqlite";

    public static void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (Exception ex) {
            System.err.println("Something went wrong closing connection to SQLite DB");
            System.err.println(ex.toString());
        }
    }
}
