package no.noroff.whytunes.data_access;

import no.noroff.whytunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackRepository {
    //Setup
    String URL = "jdbc:sqlite::resource:Chinook.sqlite";
    Connection conn = null;

    /**
     * Select some tracks from DB
     *
     * @return
     */
    public ArrayList<Track> selectTracks(String query) {
        ArrayList<Track> tracks = new ArrayList<Track>();

        try {
            // Open connection
            conn = DriverManager.getConnection(URL);

            // Get SQL query
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet results = ps.executeQuery();

            while (results.next()) {
                Track t = new Track(results);
                tracks.add(t);
            }
        } catch (Exception ex) {
            System.err.println("Error occurred.");
            System.err.print(ex.toString());
        } finally {
            SQLHelper.closeConnection(conn);
        }
        return tracks;
    }

    public ArrayList<Track> selectRandomTracks(int amount){
        return selectTracks("SELECT * from Track ORDER BY RANDOM() LIMIT "+amount);
    }

    public ArrayList<Track> selectTracksByName(String pattern){
        return selectTracks("SELECT * from Track WHERE Name LIKE '%"+pattern+"%'");
    }

    public Track selectTrack(int id){
        return selectTracks("SELECT * from Track WHERE TrackId = "+id).get(0);
    }


}
