package no.noroff.whytunes.models;

import java.sql.ResultSet;

public class Customer {
    // Identity
    int CustomerId;
    String FirstName;
    String LastName;

    String Company;
    // Location
    String Address;
    String City;
    String State;
    String Country;
    String PostalCode;
    // Contact
    String Phone;
    String Fax;
    String Email;
    int SupportRepId;

    public void applySupportId(){
        // Hardcoded for now. Should be random among employees.
        setSupportRepId(1);
    }

    public Customer(){
        //Inits nothing.
    }

    public Customer(int customerId, String firstName, String lastName, String company, String address, String city, String state, String country, String postalCode, String phone, String fax, String email) {
        CustomerId = customerId;
        FirstName = firstName;
        LastName = lastName;
        Company = company;
        Address = address;
        City = city;
        State = state;
        Country = country;
        PostalCode = postalCode;
        Phone = phone;
        Fax = fax;
        Email = email;
    }

    public Customer(String customerId,
                    String firstName,
                    String lastName,
                    String country,
                    String postalCode,
                    String phone,
                    String email){
        CustomerId = Integer.parseInt(customerId);
        FirstName = firstName;
        LastName = lastName;
        Country = country;
        PostalCode = postalCode;
        Phone = phone;
        Email = email;
        System.out.println("Made Customer:\n"+this.toString());
    }

    public Customer(ResultSet row){
        try {
            // Identity
            CustomerId = Integer.parseInt(row.getString("CustomerId"));
            FirstName = row.getString("FirstName");
            LastName = row.getString("LastName");
            // TODO: Handle company plausibly null
            //Company = row.getString("Company");
            // Location
            Address = row.getString("Address");
            City = row.getString("City");
            State = row.getString("State");
            Country = row.getString("Country");
            State = row.getString("State");
            PostalCode = row.getString("PostalCode");
            // Contact
            Phone = row.getString("Phone");
            Fax = row.getString("Fax");
            Email = row.getString("Email");
            SupportRepId = Integer.parseInt(row.getString("SupportRepId"));
        }
        catch (Exception e){
            System.err.println("Error occurred making Customer object:");
            System.err.println(e);
            System.err.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Customer{" +
                "CustomerId=" + CustomerId +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", Company='" + Company + '\'' +
                ", Address='" + Address + '\'' +
                ", City='" + City + '\'' +
                ", State='" + State + '\'' +
                ", Country='" + Country + '\'' +
                ", PostalCode='" + PostalCode + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Fax='" + Fax + '\'' +
                ", Email='" + Email + '\'' +
                ", SupportRepId=" + SupportRepId +
                '}';
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String customerId){
        CustomerId = Integer.parseInt(customerId);
    }

    /*public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }*/

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Integer getSupportRepId() {
        return SupportRepId;
    }

    public void setSupportRepId(Integer supportRepId) {
        SupportRepId = supportRepId;
    }
}
