package no.noroff.whytunes.models;

import java.util.Date;

public class Employee {
    int EmployeeId;
    String FirstName;
    String LastName;
    String Title;
    int ReportsTo;
    Date BirthDate;
    Date HireDate;
    String Address;

    public Employee(int employeeId, String firstName, String lastName, String title, int reportsTo, Date birthDate, Date hireDate, String address) {
        EmployeeId = employeeId;
        FirstName = firstName;
        LastName = lastName;
        Title = title;
        ReportsTo = reportsTo;
        BirthDate = birthDate;
        HireDate = hireDate;
        Address = address;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "EmployeeId=" + EmployeeId +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", Title='" + Title + '\'' +
                ", ReportsTo=" + ReportsTo +
                ", BirthDate=" + BirthDate +
                ", HireDate=" + HireDate +
                ", Address='" + Address + '\'' +
                '}';
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getReportsTo() {
        return ReportsTo;
    }

    public void setReportsTo(int reportsTo) {
        ReportsTo = reportsTo;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(Date birthDate) {
        BirthDate = birthDate;
    }

    public Date getHireDate() {
        return HireDate;
    }

    public void setHireDate(Date hireDate) {
        HireDate = hireDate;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}
