package no.noroff.whytunes.models;

import java.util.Date;

public class Invoice {
    Integer InvoiceId;
    Integer CustomerId;
    Date InvoiceDate;
    String BillingAddress;
    String BillingCity;

    public Invoice(Integer invoiceId, Integer customerId, Date invoiceDate, String billingAddress, String billingCity) {
        InvoiceId = invoiceId;
        CustomerId = customerId;
        InvoiceDate = invoiceDate;
        BillingAddress = billingAddress;
        BillingCity = billingCity;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "InvoiceId=" + InvoiceId +
                ", CustomerId=" + CustomerId +
                ", InvoiceDate=" + InvoiceDate +
                ", BillingAddress='" + BillingAddress + '\'' +
                ", BillingCity='" + BillingCity + '\'' +
                '}';
    }

    public Integer getInvoiceId() {
        return InvoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        InvoiceId = invoiceId;
    }

    public Integer getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(Integer customerId) {
        CustomerId = customerId;
    }

    public Date getInvoiceDate() {
        return InvoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        InvoiceDate = invoiceDate;
    }

    public String getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        BillingAddress = billingAddress;
    }

    public String getBillingCity() {
        return BillingCity;
    }

    public void setBillingCity(String billingCity) {
        BillingCity = billingCity;
    }

}
