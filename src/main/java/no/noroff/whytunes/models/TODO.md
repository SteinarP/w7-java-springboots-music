#Home page:
## Home view
- [ ] 5 random artists
- [ ] 5 random tracks
- [ ] 5 genres

## Search
- [ ] Search bar (case insensitive)
- [ ] Search result rows: 
    Track name, artist, album, year, genre

## Customer REST JSON: /api/customers
- [x] 0 Read all customers:
    id, fname, lname, country, postal, phone
- [x] 1 Read specific customer
- [x] 2 Add new customer to DB
    Random or hardcoded employee
- [x] 3 Update existing customer
- [ ] 4 Return the number of customers in each country
    Ordered descending, (top list of countries)
- [ ] 5 Top spending customers
    Order desc, total invoice table
- [ ] 6 A customer's favorite genre
    Most tracks from invoices
- [ ] Postman API call tests
    for each, exported to JSON

### Finally
- [ ] Make Readme.md
- [ ] Publish application to Heroku



