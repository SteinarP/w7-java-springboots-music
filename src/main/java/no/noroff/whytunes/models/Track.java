package no.noroff.whytunes.models;

import java.sql.ResultSet;

public class Track {
    private int TrackId;
    private String Name;
    private int AlbumId;
    private int MediaTypeId;
    private int GenreId;
    private String Composer;

    public Track(int trackId, String name, int albumId,
                 int mediaTypeId, int genreId, String composer,
                 int milliseconds, int bytes, double unitPrice) {
        TrackId = trackId;
        Name = name;
        AlbumId = albumId;
        MediaTypeId = mediaTypeId;
        GenreId = genreId;
        Composer = composer;
        Milliseconds = milliseconds;
        Bytes = bytes;
        UnitPrice = unitPrice;
    }

    public Track(ResultSet row){
        try {
            TrackId = Integer.parseInt(row.getString("TrackId"));
            Name = row.getString("Name");
            AlbumId = Integer.parseInt(row.getString("AlbumId"));
            MediaTypeId = Integer.parseInt(row.getString("MediaTypeId"));
            GenreId = Integer.parseInt(row.getString("GenreId"));
            Composer = row.getString("Composer");
            Milliseconds = Integer.parseInt(row.getString("Milliseconds"));
            Bytes = Integer.parseInt(row.getString("Bytes"));
            UnitPrice = Double.parseDouble(row.getString("UnitPrice"));

        } catch (Exception e){
            System.err.println("Error occurred making Track object:");
            System.err.println(e);
            System.err.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Track{" +
                "TrackId=" + TrackId +
                ", Name='" + Name + '\'' +
                ", AlbumId=" + AlbumId +
                ", MediaTypeId=" + MediaTypeId +
                ", GenreId=" + GenreId +
                ", Composer='" + Composer + '\'' +
                ", Milliseconds=" + Milliseconds +
                ", Bytes=" + Bytes +
                ", UnitPrice=" + UnitPrice +
                '}';
    }

    private int Milliseconds;
    private int Bytes;
    private double UnitPrice;


    public int getTrackId() {
        return TrackId;
    }

    public void setTrackId(int trackId) {
        TrackId = trackId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getAlbumId() {
        return AlbumId;
    }

    public void setAlbumId(int albumId) {
        AlbumId = albumId;
    }

    public int getMediaTypeId() {
        return MediaTypeId;
    }

    public void setMediaTypeId(int mediaTypeId) {
        MediaTypeId = mediaTypeId;
    }

    public int getGenreId() {
        return GenreId;
    }

    public void setGenreId(int genreId) {
        GenreId = genreId;
    }

    public String getComposer() {
        return Composer;
    }

    public void setComposer(String composer) {
        Composer = composer;
    }

    public int getMilliseconds() {
        return Milliseconds;
    }

    public void setMilliseconds(int milliseconds) {
        Milliseconds = milliseconds;
    }

    public int getBytes() {
        return Bytes;
    }

    public void setBytes(int bytes) {
        Bytes = bytes;
    }

    public double getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        UnitPrice = unitPrice;
    }
}
